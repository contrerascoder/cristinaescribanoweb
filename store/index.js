const getNetw = (netw, classText) => ({
    network: netw,
    url: "https://news.vuejs.org/issues/180",
    title: "Say hi to Vite! A brand new, extremely fast development setup for Vue.",
    description: "This week, I’d like to introduce you to 'Vite', which means 'Fast'. It’s a brand new development setup created by Evan You.",
    quote: "The hot reload is so fast it\'s near instant. - Evan You",
    classNetw: classText,
    hashtags: "vuejs,vite"
})
export const state = () => ({
    networks: [
        getNetw('facebook', 'fa fa-facebook'),
        getNetw('twitter', 'fa fa-twitter')
    ]
})